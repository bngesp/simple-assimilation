/***
* Name: TestOperators
* Author: admin
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model TestOperators

/* Insert your model definition here */



global {
	
	float x_int<-0.0;
	float y_int<- 0.0;
	float zero_int<- 0.0;
	
	init{
		create mon_agent number:1{
			do connect with_name:"test" to:"localhost" port:1883;
			write "connect";
		}
		
	}
	
}


species mon_agent skills:[simple_assimilation]{
	int index <- 0;
	init {
		do clear();
	}
	reflex toPrint when:has_more_message(){
		message mess <- fetch_message();
		string s <- mess.contents;
		write s;
		list var0 <- s split_with(' ');
		write "value_sensor="+var0[1];
		float t <- time;
		write t;
		do data_connect data:s Tsimul:string(t);
		list<float> data<- get_data_list();
		list<float> cyl<- get_cycle_list();
		 write "data = "+data;
		 write "time = "+cyl;
		
		if(index > 3){
			string l <- string(1.0*cycle);
			float est <- estimated(l);
			list<float> p <- param();
			write "Les parametres du model :" + p;
			float a <- p[0]+p[1]*float(l);
			write "La prediction de "+l+" ="+est ;
			write "la prediction est :" +a;
			index <- 0;
		}
		index<- index+1;
	}
}



experiment test_simple_assimilation_skill{
	output{	}
}