/***
* Name: assimilationtest
* Author: admin
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model assimilationtest

/* Insert your model definition here */

global{
	init{
		create testAgent number:2{
			//name_sub<-name+"_"+string(rnd(200));
			do connect with_name:"temp" to:"localhost" port:1883;
			//do listen to:name_sub var:my_var_name;
			do listen("humidite","humidite");
			do estimation();
		}
		
	}
	
}

species testAgent skills:[assimiation_list]{
	int every<-0;  
	//string name_sub<- "humidite";
	float humidite;// <- "vide";
	string my_var_name<- "my_var_for_"+name;
	
	
	reflex data_connect{	
		//write "j'ai un nouveau message moi !!";
		//write real_data;
		//write 
		
		
	}


	action estimation{
		write "la list "+topic_var_name;
	}
	
	action correction{
		//write "action correction de "+name_sub+" data :"+ real_data;
		write "ici "+ real_data;
		write "data list is :"+ data_from_var("humidite");
		//write "time list is :"+ time_from_var(name);
		//write "value on var_name : " + my_var_name;
		write "value on humidite : " + humidite;
		
		
	}
	
	
}

experiment test_assimilation_skill{
	output{	}

}