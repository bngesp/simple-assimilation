/***
* Name: simpleassimilation
* Author: 
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model simpleassimilation

/* Insert your model definition here */


global{
	
	init{
		create RTAgent number:1{
			do connect to:"localhost" with_name:"test" port:1883;
			//do join_group with_name:"test" ;
			write "connexion";
		}
	}
}

species RTAgent skills:[network]{
	int index <- 0;
	list<float> data_sensor<- [];
	list<float> list_cycle <- [];
	regression estimator;
	
	reflex data_connector when:has_more_message()
	{	
		message mess <- fetch_message();
		string s ;
		s<- mess.contents;
		write name + " fecth this message: " + mess.contents;	
		list var0 <- s split_with(' ');
		write "value_sensor="+var0[1];
		add float(var0[1]) at:index to:data_sensor;
		add float(cycle) at:index to:list_cycle;
		index<-index+1;
		do correction;
	}
	
	action correction{
		//return 0;
		//loop i from: 0 to: length (data_sensor) - 1 {
			write "element_y= "+data_sensor;
			write "element_x= "+list_cycle;
		//}
		if(length (data_sensor) > 5){
			matrix el <- matrix([list_cycle, data_sensor]);
			estimator <- build(el);
			write "build estimator matrix => "+ estimator;
			//float une_valeur <- float(cycle);
			//write une_valeur;
			//write " => "+estimation(une_valeur);
		
		}
	}
	
	
	float estimation(float element){
		float a <- predict(estimator, [element]);
		add float(a) at:index to:data_sensor;
		add float(element) at:index to:list_cycle;
		index<-index+1;
		return a;
	}
}



experiment test_simple_assimilation{
	output{
		//display test{
	//		species dummy aspect:default;
	//	}
	}
}