/***
* Name: simpleassimilationskills
* Author: admin
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model simpleassimilationskills

/* Insert your model definition here */

global {
	
	float x_int<-0.0;
	float y_int<- 0.0;
	float zero_int<- 0.0;
	
	init{
		create mon_agent number:1{
			do connect with_name:"test" to:"localhost" port:1883;
			write "connect";
		}
		
	}
	
}


species mon_agent skills:[simple_assimilation]{
	int index <- 0;
	init {
		do clear();
	}
	reflex toPrint when:has_more_message(){
		message mess <- fetch_message();
		string s <- mess.contents;
		write s;
		list var0 <- s split_with(' ');
		write "value_sensor="+var0[1];
		float t <- time;
		write t;
		do data_connect data:s Tsimul:string(t);
		list<float> data<- get_data_list();
		list<float> cyl<- get_cycle_list();
		create dummy number:1{
			location<-{t, float(var0[1])};
		}
		if(index > 5){
			string l <- string(1.0*cycle);
			write "La prediction de "+l+" ="+ estimated(l);
			write "Les parametres du model :" + param();
			index <- 0;
			x_int<-data[0];
			y_int<-cyl[0];
			zero_int<- estimated("30.0");
		}
		index<- index+1;
	}
}


species dummy {
	
	aspect default {
		draw circle(0.5) color: #blue;
		
	} 
}

experiment test_simple_assimilation_skill{
	output{
		display test{
			species dummy aspect:default;
			graphics "new Point " {
				//draw line([{30.0, zero_int}, {x_int, y_int}]) color:#red;
			}
		}
	}
}