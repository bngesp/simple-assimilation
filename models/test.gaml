/***
* Name: test
* Author: admin
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model test

/* Insert your model definition here */

global{
	init{
		create test number:1;
	}
	
}

species test skills:[assimilation]{
	init {
		do listen to:"localhost" from:"test";
	}
	
	reflex data_connect when:false{	
		message mess <- fetch_message();
		string s ;
		s<- mess.contents;
		write name + " fecth this message: " + mess.contents;	
	}

}

experiment test_assimilation_skill{
	output{	}
}