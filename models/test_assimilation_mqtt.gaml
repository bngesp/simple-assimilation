/***
* Name: testassimilationmqtt
* Author: admin
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model testassimilationmqtt

/* Insert your model definition here
 * 
 */


global{
	int time_to_ask <- 20;
	
	init{
		create RTAgent number:1{
			do connect to:"localhost" with_name:"test" port:1883;
			write "connexion";
		}
		create dummy number:1;
		
		write time_to_ask=cycle;
	}
	
	reflex ask_value when:(time_to_ask=cycle){
		//write "la valeur"
		float une_valeur <- float(rnd(100));
		float el<-0.0;
		ask RTAgent{
			el <- estimation(une_valeur);
		}
		write "resulte of estimation "+el;
		if(el>0.0){
			create dummy number:1;
		}
		
		time_to_ask <- 2*cycle;
	}
}

species RTAgent skills:[network]{
	int index <- 0;
	list<float> data_sensor<- [];
	list<float> list_cycle <- [];
	regression estimator;
	
	reflex data_connect when:has_more_message()
	{	
		message mess <- fetch_message();
		string s ;
		s<- mess.contents;
		write name + " fecth this message: " + mess.contents;	
		list var0 <- s split_with(' ');
		write "value_sensor="+var0[1];
		add float(var0[1]) at:index to:data_sensor;
		add float(cycle) at:index to:list_cycle;
		index<-index+1;
		do correction;
	}
	
	action correction{
		write "element_y= "+data_sensor;
		write "element_x= "+list_cycle;
		
		if(length (data_sensor) > 5){
			matrix el <- matrix([data_sensor, list_cycle]);
			estimator <- build(el);
			write "build estimator matrix => "+ estimator;
		
			float une_valeur <- float(rnd(100));
			write "resulte of estimation "+estimation(une_valeur);
		
		}
	}
	
	float estimation(float element){
		
		float a <- predict(estimator, [element, cycle]);
		write "do prediction with linear regression "+a;
		return a;
	}
}
species dummy {
	
	aspect default {
		draw circle(2) color: #blue;
	} 
}

experiment test_simple_assimilation type: gui{
	output{
		display test{
			species dummy aspect:default;
		}
	}
}