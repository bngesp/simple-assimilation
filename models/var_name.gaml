/***
* Name: varname
* Author: admin
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model varname

/* Insert your model definition here */

global{
	init{
		create Tester number:1{
			do connect with_name:"temp" to:"localhost" port:1883;
			do listen("temperature","temp_variable");
			do listen("humidite","hum_variable");
		}
	}
}

species Tester skills:[assimiation_list]{
	float temp_variable;
	
	
	action correction {
		write "t_date for temp is "+t_data("temperature");
		write "t_date for hum is "+t_data("humidite");
		write "\n";
		do estimation();
	}
	
	action estimation {
		write "action call t_simul " +t_simul;
		write "list of t_date is "+t_data;
	}
}


experiment bouton{
	output{	}

}